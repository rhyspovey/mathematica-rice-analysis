(* ::Package:: *)

(* ::Title:: *)
(*RiceAnalysis*)


(* ::Text:: *)
(*Rhys G. Povey <rhyspovey@gmail.com>*)


(* ::Section:: *)
(*Front End*)


BeginPackage["RiceAnalysis`"];


RiceAnalyze::usage="RiceAnalyze[data] analyzes a one dimensional list of amplitudes assuming a source Rice distribution; or analyzes a two dimensional list of Rician parameters assuming a constant source.";


RiceAnalyze::positive="Amplitude data should be positive.";


RiceAnalyze::lowamplitude="Amplitude estimate \!\(\*OverscriptBox[\(\[Nu]\), \(^\)]\) << \!\(\*OverscriptBox[\(\[Sigma]\), \(^\)]\), information is unreliable. A large non-Gaussian region of \[Nu] parameter space may be viable.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Text:: *)
(*Analyze Rice source with exact measurements*)


RiceAnalyze[data_/;Depth[data]==2,OptionsPattern[]]:=Module[{\[Mu]raw1,\[Mu]raw2,n,\[Nu],\[Sigma],\[Zeta],\[ScriptCapitalI],log\[ScriptCapitalL],\[Gamma]},
If[Or@@(#<=0&/@data),Message[RiceAnalyze::positive]];
n=Length[data];

\[Mu]raw1=Plus@@data/n;
\[Mu]raw2=Plus@@(#^2&/@data)/n;
(* estimators *)
\[Nu]=\[FormalN]/.FindRoot[Plus@@(# BesselI[1,(# 2 \[FormalN])/(\[Mu]raw2-\[FormalN]^2)]/BesselI[0,(# 2 \[FormalN])/(\[Mu]raw2-\[FormalN]^2)]&/@data)-n \[FormalN],{\[FormalN],\[Mu]raw1}];
\[Sigma]=Sqrt[(\[Mu]raw2-\[Nu]^2)/2];
(* information *)
\[Zeta]=Sqrt[NIntegrate[(\[FormalA]^3 E^(-((\[FormalA]^2+\[Nu]^2)/(2 \[Sigma]^2))) BesselI[1,(\[FormalA] \[Nu])/\[Sigma]^2]^2)/(\[Sigma]^2 BesselI[0,(\[FormalA] \[Nu])/\[Sigma]^2]),{\[FormalA],0,\[Infinity]}]];
\[Gamma]=Sqrt[1/n Plus@@(#^2 BesselI[1,(# \[Nu])/\[Sigma]^2]^2/BesselI[0,(# \[Nu])/\[Sigma]^2]^2&/@data )]; (* Approx determination of \[Zeta] *)
\[ScriptCapitalI]=-n{{1/\[Sigma]^4 (\[Nu]^2-\[Zeta]^2),(2 \[Nu])/\[Sigma]^5 (\[Zeta]^2-\[Sigma]^2-\[Nu]^2)},{(2 \[Nu])/\[Sigma]^5 (\[Zeta]^2-\[Sigma]^2-\[Nu]^2),(4\[Nu]^2)/\[Sigma]^6 (-(\[Sigma]^4/\[Nu]^2)+\[Sigma]^2+\[Nu]^2-\[Zeta]^2)}};
(* Log likelihood *)
log\[ScriptCapitalL]=Plus@@(Log[PDF[RiceDistribution[\[Nu],\[Sigma]],#]]&/@data);
(* output *)
If[\[Nu]/\[Sigma]<OptionValue["LowAmplitudeThreshold"],Message[RiceAnalyze::lowamplitude]];
<|
"Estimate"->{\[Nu],\[Sigma]},
"FisherInformation"->\[ScriptCapitalI],
"LogLikelihood"->log\[ScriptCapitalL],
"Parameters"->{"\[Nu]","\[Sigma]"}
|>
];


(* ::Text:: *)
(*Analyze constant source with Rice measurements*)


RiceAnalyze[data_/;Depth[data]==3,OptionsPattern[]]:=Module[{\[Nu]guess,\[Nu],\[Zeta],\[ScriptCapitalI],log\[ScriptCapitalL]},
If[Or@@(#<=0&/@data),Message[RiceAnalyze::positive]];

\[Nu]guess=Plus@@(#[[1]]/#[[2]]^2 BesselI[1,#[[1]]^2/#[[2]]^2]/BesselI[0,#[[1]]^2/#[[2]]^2]&/@data)/Plus@@(1/#[[2]]^2&/@data);

(* estimators *)
\[Nu]=\[FormalN]/.FindRoot[Plus@@(#[[1]]/#[[2]]^2 BesselI[1,(#[[1]] \[FormalN])/#[[2]]^2]/BesselI[0,(#[[1]] \[FormalN])/#[[2]]^2]&/@data)- \[FormalN] Plus@@(1/#[[2]]^2&/@data),{\[FormalN],\[Nu]guess}];

(* information *)
\[Zeta][\[Sigma]_]:=Sqrt[NIntegrate[(\[FormalA]^3 E^(-((\[FormalA]^2+\[Nu]^2)/(2 \[Sigma]^2))) BesselI[1,(\[FormalA] \[Nu])/\[Sigma]^2]^2)/(\[Sigma]^2 BesselI[0,(\[FormalA] \[Nu])/\[Sigma]^2]),{\[FormalA],0,\[Infinity]}]];
\[ScriptCapitalI]=Plus@@((\[Zeta][#[[2]]]^2-\[Nu]^2)/#[[2]]^4&/@data);
(* Log likelihood *)
log\[ScriptCapitalL]=Plus@@(Log[PDF[RiceDistribution[\[Nu],#[[2]]],#[[1]]]]&/@data);
(* output *)
<|
"Estimate"->\[Nu],
"FisherInformation"->\[ScriptCapitalI],
"LogLikelihood"->log\[ScriptCapitalL],
"Parameters"->"\[Nu]"
|>
];


Options[RiceAnalyze]={"LowAmplitudeThreshold"->0.01};


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
